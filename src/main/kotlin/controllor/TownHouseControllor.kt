package controllor

import domain.FamilyDomain
import io.quarkus.vertx.web.Body
import io.quarkus.vertx.web.Route
import io.quarkus.vertx.web.RoutingExchange
import jakarta.enterprise.context.ApplicationScoped
import jakarta.ws.rs.core.MediaType
import service.TownHouseService

@ApplicationScoped
class TownHouseControllor(val townHouseService: TownHouseService) {

    @Route(
        methods = [Route.HttpMethod.POST],
        path = "townHouse/addFamily",
        consumes = [MediaType.APPLICATION_JSON],
        produces = [MediaType.APPLICATION_JSON]
    )
    fun addFamily(re: RoutingExchange, @Body familyDomain: FamilyDomain) {
        townHouseService.addFamily(familyDomain)
        re.response().setStatusCode(200).end("OK")
    }

    @Route(
        methods = [Route.HttpMethod.GET],
        produces = [MediaType.APPLICATION_JSON],
        path = "townHouse/getAllFamily",
    )
    fun allFamily():List<FamilyDomain> {
        return townHouseService.allFamily()
    }
}