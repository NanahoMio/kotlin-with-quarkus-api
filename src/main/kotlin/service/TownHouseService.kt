package service

import Enum.Sex
import domain.FamilyDomain
import domain.PersonDomain
import jakarta.enterprise.context.ApplicationScoped

@ApplicationScoped
class TownHouseService {

    private val listFamily = mutableListOf(
        FamilyDomain("Seedanoi", "Hatyai", mutableListOf(PersonDomain("Patiparn Seedanoi", "Pele", 24, Sex.Male.name, "Dev code")), 100000.00)
    )

    fun addFamily(familyDomain: FamilyDomain) {
        listFamily.add(familyDomain)
    }

    fun allFamily():List<FamilyDomain> {
        return listFamily
    }
}