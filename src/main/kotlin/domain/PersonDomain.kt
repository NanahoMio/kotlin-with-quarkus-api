package domain

import io.quarkus.runtime.annotations.RegisterForReflection

@RegisterForReflection
data class PersonDomain(
    val fullName: String,
    val nickname: String,
    val age: Int,
    val sex: String,
    val work: String?
)
