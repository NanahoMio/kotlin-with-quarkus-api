package domain

data class FamilyDomain(
    val familyName: String,
    val locationHome: String,
    val personInFamily: List<PersonDomain>,
    val money: Double
)
